import { expect } from 'chai';

import chalk from 'chalk';

import { highlightText } from '../../src/coloring.mjs';

const { white, red, yellow, green, cyan } = chalk;

describe('src/coloring', () => {
    describe('highlightText', () => {
        it('Should paint a number white', () => {
            const expected = `Number ${white('[112358]')} is a fibonacci seq.`;
            const actual = highlightText('Number [112358] is a fibonacci seq.');
            expect(actual).to.be.equal(expected);
        });

        it('Should paint the string "rejected" red', () => {
            const expected = `The word ${red('rejected')} must be red.`;
            const actual = highlightText('The word rejected must be red.');
            expect(actual).to.be.equal(expected);
        });

        it('Should paint the string "skipped" yellow', () => {
            const expected = `The word ${yellow('skipped')} must be yellow.`;
            const actual = highlightText('The word skipped must be yellow.');
            expect(actual).to.be.equal(expected);
        });

        it('Should paint the string "accepted" green', () => {
            const expected = `The word ${green('accepted')} must be green.`;
            const actual = highlightText('The word accepted must be green.');
            expect(actual).to.be.equal(expected);
        });

        it('Should paint the string "saved" green', () => {
            const expected = `The word ${green('saved')} must be green.`;
            const actual = highlightText('The word saved must be green.');
            expect(actual).to.be.equal(expected);
        });

        it('Should paint double quoted strings cyan', () => {
            const expected = `The file ${cyan('"test.txt"')} was created.`;
            const actual = highlightText('The file "test.txt" was created.');
            expect(actual).to.be.equal(expected);
        });
    });
});