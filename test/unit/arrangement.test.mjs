import { expect } from 'chai';

import { quote, makeLabel, makeKeyValuePairs } from '../../src/arrangement.mjs';

describe('src/arrangement', () => {
    describe('makeLabel', () => {
        it('Should concatenate a level and a logger name', () => {
            const actual = makeLabel('test logger', 'fake level');
            const expected = '[fake level] [test logger]';
            expect(actual).to.be.equal(expected);
        });
    });

    describe('makeKeyValuePairs', () => {
        it('Should return an empty array given an empty input', () => {
            const actual = makeKeyValuePairs();
            expect(actual).to.be.empty;
        });

        it('Should return an one element for each input pair', () => {
            const actual = makeKeyValuePairs(['key1', 1, 'key2', 2, 'key3']);
            const expected = ['key1=1', 'key2=2'];
            expect(actual).to.be.deep.equal(expected);
        });
    });

    describe('quote', () => {
        it('Should return a quoted/escaped string', () => {
            const actual = quote('this is a \'test\' string');
            const expected = '\'this is a \\\'test\\\' string\'';
            expect(actual).to.deep.equal(expected);
        });

        it('Should return a string representation of a number', () => {
            const actual = quote(12345);
            const expected = '12345';
            expect(actual).to.deep.equal(expected);
        });

        it('Should return a comma separated list of quoted strings', () => {
            const actual = quote(['a', 'b', 'c']);
            const expected = '[\'a\',\'b\',\'c\']';
            expect(actual).to.deep.equal(expected);
        });

        it('Should return the string representation of an object', () => {
            const actual = quote({});
            const expected = '[object Object]';
            expect(actual).to.deep.equal(expected);
        });
    });
});