import { expect, use } from 'chai';

import sinon from 'sinon';
import { makeLineContent } from '../../src/formating.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();
const dummyMakeKeyValuePairs = sandbox.stub();
const dummyQuote = sandbox.stub();
const dummyHighlightText = sandbox.stub();

describe('src/formating', () => {
    describe('makeLineContent', () => {
        it('Should assemble a line content', () => {
            dummyHighlightText.onFirstCall().returns('highlighted message');
            dummyHighlightText.onCall(1).returns('highlighted 1');
            dummyHighlightText.onCall(2).returns('highlighted 2');
            dummyHighlightText.onCall(3).returns('highlighted 3');

            dummyQuote.onFirstCall().returns('quoted message');

            dummyMakeKeyValuePairs.onFirstCall().returns([1, 2, 3]);

            const expected = 'message=quoted message highlighted 1 highlighted 2 highlighted 3';
            const actual = makeLineContent(
                'some message',
                [1, 2, 3],
                dummyMakeKeyValuePairs,
                dummyQuote,
                dummyHighlightText);

            expect(actual).to.be.equal(expected);
            expect(dummyHighlightText).to.be.calledWith(1);
            expect(dummyHighlightText).to.be.calledWith(2);
            expect(dummyHighlightText).to.be.calledWith(3);
            expect(dummyHighlightText).to.be.calledWith('some message');
            expect(dummyQuote).to.be.calledWith('highlighted message');
            expect(dummyMakeKeyValuePairs).to.be.calledWith([1, 2, 3]);
        });
    });
});