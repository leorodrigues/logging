
export function makeLineContent(
    message = '',
    context = [ ],
    makeKeyValuePairs = () => [ ],
    quote = i => i,
    highlightText = i => i
) {
    const content = [
        `message=${quote(highlightText(message))}`,
        ...(makeKeyValuePairs(context, quote).map(p => highlightText(p)))
    ];

    return content.join(' ');
}