import chalk from 'chalk';

const { green, yellow, magenta, cyan, white, red } = chalk;

export function highlightText(message) {
    return message
        .replace(/"[^"]*"/ig, t => cyan(t))
        .replace(/saved/ig, t => green(t))
        .replace(/accepted/ig, t => green(t))
        .replace(/skipped/ig, t => yellow(t))
        .replace(/rejected/ig, t => red(t))
        .replace(/[a-z0-9]{40}/ig, t => magenta(t))
        .replace(/\[\d+(\.\d+)?\]/g, t => white(t));
}

export function highlightLine(level, line) {
    switch (level) {
        case 0: return red(line);
        case 1: return yellow(line);
        case 3: return cyan(line);
        default: return line;
    }
}
