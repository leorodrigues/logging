
export function makeLabel(loggerName, level) {
    return `[${level}] [${loggerName}]`;
}

export function makeKeyValuePairs(
    elements = [],
    quote = i => i,
    products = []
) {
    const [key, value, ...reminder] = elements;
    if (value === undefined) return products;
    products.push(`${key}=${quote(value)}`);
    return makeKeyValuePairs(reminder, quote, products);
}

export function quote(input) {
    if (typeof input === 'string') return `'${input.replaceAll('\'', '\\\'')}'`;
    if (Array.isArray(input)) return `[${input.map(o => quote(o)).join(',')}]`;
    return input.toString();
}
