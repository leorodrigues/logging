
const LEVELS = [ 'error', 'warning', 'info', 'debug' ];

const LEVEL_DEBUG = LEVELS.indexOf('debug');
const LEVEL_WARNING = LEVELS.indexOf('warning');
const LEVEL_ERROR = LEVELS.indexOf('error');
const LEVEL_INFO = LEVELS.indexOf('info');

export class InvalidLogLevelError extends Error {
    constructor(level) {
        super(`There is no such log level. level=${level}`);
    }
}

export class Logger {
    constructor(name, level, printer, context) {
        confirmValidLogLevel(LEVELS, level);
        Object.assign(this, {
            level: LEVELS.indexOf(level), name, context, printer
        });
    }

    info(message, ...context) {
        this.printer.printLine(
            LEVELS, new Date(), LEVEL_INFO, this.level, this.name, message,
            this.context, context
        );
    }

    warn(message, ...context) {
        this.printer.printLine(
            LEVELS, new Date(), LEVEL_WARNING, this.level, this.name, message,
            this.context, context
        );
    }

    warning(message, ...context) {
        this.printer.printLine(
            LEVELS, new Date(), LEVEL_WARNING, this.level, this.name, message,
            this.context, context
        );
    }

    error(message, ...context) {
        this.printer.printLine(
            LEVELS, new Date(), LEVEL_ERROR, this.level, this.name, message,
            this.context, context
        );
    }

    debug(message, ...context) {
        this.printer.printLine(
            LEVELS, new Date(), LEVEL_DEBUG, this.level, this.name, message,
            this.context, context
        );
    }

    setLevel(level) {
        confirmValidLogLevel(LEVELS, level);
        this.level = LEVELS.indexOf(level);
    }

    newChildWithContext(name, { level, context } = { }) {
        const childName = `${this.name}.${name}`;
        const resolvedLevel = level || LEVELS[this.level];
        const resolvedContext = mergeContext(this.context, context || [ ]);

        return new Logger(
            childName, resolvedLevel, this.printer, resolvedContext);
    }

    newWithContext(name, { level, context } = { }) {
        const resolvedLevel = level || LEVELS[this.level];
        const resolvedContext = mergeContext(this.context, context || [ ]);

        return new Logger(
            name, resolvedLevel, this.printer, resolvedContext);
    }

    static get LEVEL_ERROR() {
        return 'error';
    }

    static get LEVEL_INFO() {
        return 'info';
    }

    static get LEVEL_WARNING() {
        return 'warning';
    }

    static get LEVEL_DEBUG() {
        return 'debug';
    }
}

function confirmValidLogLevel(levels, level) {
    if (levels.includes(level)) return;
    throw new InvalidLogLevelError(level);
}

function mergeContext(first = [ ], second = [ ]) {

    function *pairs(...array) {
        const max = Math.floor(array.length / 2);
        for (let i = 0; i < max; i++)
            yield array.splice(0, 2);
    }

    const firstPairs = [...pairs(...first)];
    const secondPairs = [...pairs(...second)];

    for (const f of firstPairs)
        for (const s of secondPairs)
            if (f[0] === s[0])
                f[1] = s[1];

    return [
        ...firstPairs,
        ...secondPairs.filter(s => !firstPairs.some(f => f[0] === s[0]))
    ].flatMap(p => p);
}
