
import { makeLabel, makeKeyValuePairs, quote } from './arrangement.mjs';
import { highlightLine } from './coloring.mjs';
import { makeLineContent } from './formating.mjs';

const functions = [ makeKeyValuePairs, quote ];

export class ColoredPrinter {
    printLine(
        levels, date, level, baseLevel, name, message, baseContext, context
    ) {
        if (baseLevel < level) return;
        const fullContext = [...baseContext, ...context];
        const content = makeLineContent(message, fullContext, ...functions);
        const label = makeLabel(name, levels[level]);
        const timestamp = date.toISOString();
        const line = `[${timestamp}] ${label}: ${content}`;
        console.log(highlightLine(level, line));
    }
}

export class MonochromePrinter {
    printLine(
        levels, date, level, baseLevel, name, message, baseContext, context
    ) {
        if (baseLevel < level) return;
        const fullContext = [...baseContext, ...context];
        const content = makeLineContent(message, fullContext, ...functions);
        const label = makeLabel(name, levels[level]);
        const timestamp = date.toISOString();
        const line = `[${timestamp}] ${label}: ${content}`;
        console.log(line);
    }
}