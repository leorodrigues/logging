import makeLogger from './index.mjs';
import { Logger } from './src/logger.mjs';

const log = makeLogger('showcase', {
    context: ['globalKey', 'global-value'],
    level: Logger.LEVEL_ERROR
});

log.info('an event has happened',
    'key1', 'there is a "file" to be sent',
    'key2', 112358,
    'status', 'rejected');

log.error('an event has happened',
    'key1', 'value 1',
    'key2', '[112358]',
    'status', 'skipped');

log.warn('an event has happened', 'key1', 'value 1',
    'key2', 112358,
    'status', 'accepted');

log.debug('an event has happened', 'key1', 'value 1',
    'key2', 112358,
    'status', 'accepted');

log.setLevel(Logger.LEVEL_DEBUG);

log.info('an event has happened',
    'key1', 'there is a "file" to be sent',
    'key2', 112358,
    'status', 'rejected');

log.error('an event has happened',
    'key1', 'value 1',
    'key2', '[112358]',
    'status', 'skipped');

log.warn('an event has happened', 'key1', 'value 1',
    'key2', 112358,
    'status', 'accepted');

log.debug('an event has happened', 'key1', 'value 1',
    'key2', 112358,
    'status', 'accepted');

const childLogger = log.newChildWithContext('child', { context: [
    'globalKey', 42
] });

childLogger.info('Hello.', 'who', 'world');
log.info('Hello.', 'who', 'world');

