
import { Logger } from './src/logger.mjs';
import { ColoredPrinter } from './src/printing.mjs';

export { ColoredPrinter, MonochromePrinter } from './src/printing.mjs';

export default function makeLogger(name, { level, printer, context } = { }) {
    let resolvedLevel = (level || process.env.LOG_LEVEL || Logger.LEVEL_INFO);
    const resolvedName = name || 'default-logger';
    const resolvedPrinter = printer || new ColoredPrinter();
    const resolvedContext = context || [ ];
    resolvedLevel = resolvedLevel.toLowerCase();

    return new Logger(
        resolvedName,
        resolvedLevel,
        resolvedPrinter,
        resolvedContext);
};
